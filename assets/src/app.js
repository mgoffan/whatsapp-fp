// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import css from "./styles/app.css"

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import dependencies
//
import 'phoenix_html';

// Import local files
//
// Local files can be imported directly using relative paths, for example:
import socket from './socket';
import { Elm } from './Main.elm';

const senderAdapter = s => ({
  id: s.id,
  name: s.name,
  username: s.username
});

const messageAdapter = m => ({
  id: m.id,
  content: m.content,
  timestamp: +new Date(m.timestamp),
  sender: senderAdapter(m.sender)
});

const conversationAdapter = c => ({
  id: c.id,
  title: c.title,
  iconUrl: c.iconUrl,
  messages: c.messages.map(messageAdapter),
  created: +new Date(c.created)
});

const LOCAL_STORAGE_KEY = 'whatsapp-fp:participant';

const participant = parseInt(localStorage.getItem(LOCAL_STORAGE_KEY) || null, 10) || null;

let channel;

console.log('participant', participant);

const initializeElm = (conversations = []) => {
  const app = Elm.Main.init({
    flags: JSON.stringify({
      participant,
      conversations
    })
  });

  const openConversations = {}

  app.ports.toJs.subscribe(({ command, attrs }) => {
    const [action, param] = command.split(':');
    console.log(command, attrs);
    if (action === 'conversation') {
      const id = parseInt(param, 10);
      if (openConversations[id]) return;
      const conversationChannel = socket.channel(command, {});
      conversationChannel.on('messages', ({ messages }) => {
        console.log(messages);
        
        app.ports.toElm.send(JSON.stringify({
          command: 'load_messages',
          payload: {
            id,
            messages: messages.map(messageAdapter)
          }
        }));
      });
      conversationChannel.on('message', ({ message }) => {
        const data = {
          command: 'message',
          payload: {
            id,
            message: messageAdapter(message)
          }
        };
        console.log('rcvd message', data);
        app.ports.toElm.send(JSON.stringify(data));
      });
      openConversations[id] = 1;
      conversationChannel.join();
      return;
    }
    if (action === 'create_conversation') {
      channel.push('create_conversation', attrs);
      return;
    }
    if (action === 'send_message') {
      channel.push('send_message', attrs);
      return;
    }
    if (action === 'login') {
      
      const registerChannel = socket.channel(`register:${attrs.username}`, {});

      registerChannel.on('init', ({ user_id }) => {
        console.log('login', user_id);
        localStorage.setItem('whatsapp-fp:participant', user_id);

        channel = socket.channel(`participant:${user_id}`, {});

        channel.on('init', ({ conversations }) => {
          console.log('init', conversations);
          const adaptedConversations = conversations.map(conversationAdapter);

          const data = {
            command: 'login',
            payload: {
              id: user_id,
              conversations: adaptedConversations
            }
          };

          console.log(JSON.stringify(data));

          app.ports.toElm.send(JSON.stringify(data));

          // initializeElm(adaptedConversations);
        });

        channel.join();

        registerChannel.leave();
      });

      registerChannel.join();      
    }
  });

  const bindCreateConversation = channel => {
    if (!channel) return;

    channel.on('create_conversation', ({ conversation }) => {
      const data = {
        command: 'create_conversation',
        payload: conversationAdapter(conversation)
      };
      console.log('rcvd create_conversation', data);
      app.ports.toElm.send(JSON.stringify(data));
    });
  };

  bindCreateConversation(channel);
}

if (participant) {
  channel = socket.channel(`participant:${participant}`, {});

  channel.on('init', ({ conversations }) => {
    console.log('init', conversations);
    const adaptedConversations = conversations.map(conversationAdapter);
    initializeElm(adaptedConversations);
  });

  channel.join(); // join the channel.
} else {
  initializeElm();
}
