module Containers.ConversationMessages exposing (..)

import Html exposing (Html, div, span, text)
import Html.Attributes exposing (class)
import Types.App exposing (Model, Msg)
import Types.Model exposing (Conversation)

import Components.ConversationMessage exposing (conversationMessage)


conversationMessages : Conversation -> Html Msg
conversationMessages conversation =
    div [ class "conversation-messages" ]
        (List.map conversationMessage conversation.messages)