module Containers.Conversation exposing (..)

import Html exposing (Html, div, span, text)
import Html.Attributes exposing (class)
import Types.App exposing (Model, Msg)

import Components.ConversationTopBar exposing (conversationTopBar)
import Components.ConversationInput exposing (conversationInput)
import Containers.ConversationMessages exposing (conversationMessages)

conversationContainer : Model -> Html Msg
conversationContainer model =
    let
        maybeConversation = List.filter (\c -> c.selected == True) model.conversations |> List.head
    in
        case maybeConversation of
            Nothing ->
                div [ class "conversation empty" ] []
            Just conversation ->
                div [ class "conversation" ]
                    [ conversationTopBar conversation
                    , conversationMessages conversation
                    , conversationInput conversation
                    ]