module Containers.ChatLayout exposing (..)

import Html exposing (Html, div)
import Html.Attributes exposing (class)
import Types.App exposing (Model, Msg)

import Components.ActionBar exposing (actionBar)
import Components.SearchBar exposing (searchBar)

import Containers.Conversation exposing (conversationContainer)
import Containers.ConversationList exposing (conversationList)

chatLayout : Model -> Html Msg
chatLayout model =
    div [ class "container"]
        [ div [ class "left-pane" ]
            [ actionBar model
            , searchBar model
            , conversationList model
            ]
        , div [ class "right-pane"]
            [ conversationContainer model ]
        ]