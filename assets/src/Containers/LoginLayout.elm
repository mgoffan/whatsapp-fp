module Containers.LoginLayout exposing (..)

import Html exposing (Html, div)
import Html.Attributes exposing (class)
import Types.App exposing (Model, Msg)

import Components.LoginForm exposing (loginForm)

loginLayout : Model -> Html Msg
loginLayout model =
    div [ class "container login"]
        [ div [ class "login-form-container" ]
            [ loginForm model ]
        ]