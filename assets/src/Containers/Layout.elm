module Containers.Layout exposing (..)

import Html exposing (Html, div)
import Html.Attributes exposing (class)
import Types.App exposing (Model, Msg)

import Containers.ChatLayout exposing (chatLayout)
import Containers.LoginLayout exposing (loginLayout)

layout : Model -> Html Msg
layout model =
    case model.participant of
        Just p ->
            chatLayout model
        Nothing ->
            loginLayout model
