module Containers.ConversationList exposing (..)

import Html exposing (Html, div, span, text)
import Html.Attributes exposing (class)
import Types.App exposing (Model, Msg)
import Types.Model exposing (Conversation)
import Regex exposing (..)

import Components.ConversationItem exposing (conversationItem)

makeRegex : String -> Regex
makeRegex query =
    let
        regex = Regex.fromStringWith { caseInsensitive = True, multiline = False } query
    in
        case regex of
            Just r -> r
            Nothing -> Regex.never

filterPredicate : String -> Conversation -> Bool
filterPredicate query conversation = Regex.contains (makeRegex query) conversation.title
    
lastMessage : Conversation -> Int
lastMessage c =
    case List.reverse c.messages |> List.head of
        Just m -> -m.timestamp
        Nothing -> 0

conversationList : Model -> Html Msg
conversationList model =
    div [ class "conversation-list" ]
        (List.map conversationItem
            (List.sortBy lastMessage
                ( List.filter (filterPredicate model.query) model.conversations)
            )
        )