module Components.ConversationItem exposing (..)

import Html exposing (Html, div, span, text, img, button)
import Html.Attributes exposing (class, src)
import Html.Events exposing (onClick)
import Types.App exposing (Msg(..))
import Types.Model exposing (Conversation)

import Time exposing (utc, toHour, toMinute, toSecond, toYear, toDay, millisToPosix)

import String exposing (fromInt)

-- toMonthNumber : Time.Month -> String
-- toMonthNumber month =
--     case month of
--         Jan -> "01"
--         Feb -> "02"
--         Mar -> "03"
--         Apr -> "04"
--         May -> "05"
--         Jun -> "06"
--         Jul -> "07"
--         Aug -> "08"
--         Sep -> "09"
--         Oct -> "10"
--         Nov -> "11"
--         Dec -> "11"

formatTimestamp : Int -> String
formatTimestamp ts =
    String.fromInt (toHour utc (millisToPosix ts))
    ++ ":" ++
    String.fromInt (toMinute utc (millisToPosix ts))

conversationItem : Conversation -> Html Msg
conversationItem conversation =
    let
        iconUrl = Maybe.withDefault "http://www.ra.ac.ae/wp-content/uploads/2017/02/user-icon-placeholder.png" conversation.iconUrl
        
        lastMessage = case List.reverse conversation.messages |> List.head of
            Just message -> message.content
            Nothing      -> ""
        
        timestamp = case List.reverse conversation.messages |> List.head of
            Just message -> formatTimestamp message.timestamp
            Nothing      -> formatTimestamp conversation.created

    in
        div [ class ("conversation-item" ++ if conversation.selected then " active" else "")
            , onClick (SelectConversation conversation)
            ]
            [ div [ class "left" ]
                [ img [ src iconUrl, class "profile-pic"] []
            ]
            , div [ class "body" ]
                [ span [ class "title" ] [ text conversation.title ]
                , span [ class "last-message" ] [ text lastMessage]
                , if (String.isEmpty conversation.currentMessage)
                    then
                        span [] []
                    else
                        span [ class "badge" ] [ text "Editing" ]
                ]
            , div [ class "right" ]
                [ span
                    [ class "time" ]
                    [ text timestamp ]
                -- , span [ class "badge" ] [ text (String.fromInt conversation.unreadCount) ]
                ]
            ]
            