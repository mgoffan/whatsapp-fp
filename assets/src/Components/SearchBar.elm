module Components.SearchBar exposing (..)

import Html exposing (Html, div, span, text, input)
import Html.Attributes exposing (class, value, placeholder)
import Html.Events exposing (onInput)
import Types.App exposing (Model, Msg(..))

searchBar : Model -> Html Msg
searchBar model =
    div [ class "search-bar" ]
        [ div [ class "input-container"]
            [ input
                [ onInput OnQueryChange
                , value model.query
                , placeholder "Search.."
                ] []
            ]
        ]