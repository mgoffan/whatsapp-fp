module Components.ConversationTopBar exposing (..)

import Html exposing (Html, div, span, text)
import Html.Attributes exposing (class)
import Types.App exposing (Model, Msg)
import Types.Model exposing (Conversation)

conversationTopBar : Conversation -> Html Msg
conversationTopBar conversation =
    div [ class "conversation-top-bar" ]
        [ span [ class "title" ] [ text conversation.title ]
        ]
            