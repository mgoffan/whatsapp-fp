module Components.ConversationMessage exposing (..)

import Html exposing (Html, div, span, text, b)
import Html.Attributes exposing (class)
import Types.App exposing (Model, Msg)
import Types.Model exposing (Message)

conversationMessage : Message -> Html Msg
conversationMessage message =
    div [ class "message" ]
        [ b [] [ text (message.sender.username ++ ": ") ]
        , span [] [ text message.content ]
        ]