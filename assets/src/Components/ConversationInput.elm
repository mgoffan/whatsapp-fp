module Components.ConversationInput exposing (..)

import Browser exposing (sandbox)
import Html exposing (Html, div, button, input, text)
import Html.Attributes exposing (class, value, placeholder)
import Html.Events exposing (onClick, onInput)
import Types.App exposing (Model, Msg(..))
import Types.Model exposing (Conversation)

conversationInput : Conversation -> Html Msg
conversationInput conversation =
    div [ class "conversation-input" ]
        [ div [ class "input-container"]
            [ input
                [ onInput (OnChange conversation)
                , value conversation.currentMessage
                , placeholder "Type a message..."
                ] [] ]
        , button [ onClick (SendMessage conversation )] [ text "Send"]
        ]
