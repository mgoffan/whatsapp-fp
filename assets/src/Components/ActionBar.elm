module Components.ActionBar exposing (..)

import Html exposing (Html, div, span, text, button)
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)
import Types.App exposing (Model, Msg(..))

import Components.NewChatModal exposing (newChatModal)

actionBar : Model -> Html Msg
actionBar model =
    div [ class "action-bar" ]
        [ span [ class "action-bar-title" ] [ text "Whatsapp FP" ]
        , button
            [ onClick DisplayNewChatModal
            , class "action-bar-create-button"
            ] [ text "+"]
        , newChatModal model
        ]