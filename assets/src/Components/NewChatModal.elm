module Components.NewChatModal exposing (..)

import Browser exposing (sandbox)
import Html exposing (Html, div, button, input, text, h4, form, textarea)
import Html.Attributes exposing (class, value, placeholder)
import Html.Events exposing (onClick, onInput)
import Types.App exposing (Model, Msg(..))
import Types.Model exposing (Conversation)

nothing : Html msg
nothing = text ""

modal : Html msg -> Html msg
modal children =
    div []
        [ div [ class "modal fade in" ]
            [ div [ class "modal-dialog" ]
                [ div [ class "modal-content" ]
                    [ children ]
                ]
            ]
        , div [ class "modal-backdrop fade in" ] []
        ]

newChatModal : Model -> Html Msg
newChatModal model = 
    if model.showCreateChatModal then
        let
            children =
                div []
                    [ div [ class "modal-header" ]
                        [ h4 [ class "modal-title" ] [ text "New Chat" ]
                        ]
                    , div [ class "modal-body" ]
                        [ form [ class "form" ]
                            [ input [ class "form-control", placeholder "Username", onInput OnNewMessageUserChange, value model.user ] []
                            , input [ class "form-control", placeholder "Subject", onInput OnNewMessageSubjectChange, value model.subject ] []
                            , textarea [ class "form-control", placeholder "Message", onInput OnNewMessageContentChange, value model.content ] []
                            ]
                        ]
                    , div [ class "modal-footer" ]
                        [ button [ class "default", onClick CloseNewChatModal ] [ text "Close"]
                        , button [ onClick SendNewMessage ] [ text "Send"]
                        ]
                    ]
        in
            modal children
    else
        nothing