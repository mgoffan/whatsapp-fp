module Components.LoginForm exposing (..)

import Browser exposing (sandbox)
import Html exposing (Html, div, button, text, input, form, img, h4)
import Html.Attributes exposing (class, value, placeholder, src, type_)
import Html.Events exposing (onClick, onInput, onSubmit)
import Types.App exposing (Model, Msg(..))
import Types.Model exposing (Conversation)

whatsappLogoUrl : String
whatsappLogoUrl = "https://cdn1.techadvisor.co.uk/cmsdata/features/3682764/how-to-move-whatsapp-from-iphone-to-android-main_thumb800.png"

loginForm : Model -> Html Msg
loginForm model = 
    form [ class "form", onSubmit Login ]
        [ img [ src whatsappLogoUrl ] []
        , h4 [] [text "Whatsapp FP"]
        , input [ class "form-control", placeholder "Username", onInput LoginUsernameChange, value model.username ] []
        , button [ type_ "submit" ] [text "Login"]
        ]