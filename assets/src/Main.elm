port module Main exposing (init, main, toJs, update, view)

import Browser
import Browser.Navigation as Nav
import Html exposing (Html, div)
import Html.Attributes exposing (id, class)

import Types.App exposing (Model, Msg(..))
import Types.Model exposing (conversationDecoder, messageDecoder, Conversation, Message, MessageStatus(..))
import Containers.Layout exposing (layout)

import Time exposing (..)
import Task exposing (..)
import Json.Decode as D
import Json.Encode as E
import Update.Extra as Update

-- ---------------------------
-- PORTS
-- ---------------------------
port toJs : E.Value -> Cmd msg
port toElm : (String -> msg) -> Sub msg

-- ---------------------------
-- INIT
-- ---------------------------

type alias InitialFlags =
    { participant: Maybe Int
    , conversations: List Conversation
    }

initialFlagsDecoder : D.Decoder InitialFlags
initialFlagsDecoder =
    D.map2 InitialFlags
        (D.field "participant" (D.nullable D.int))
        (D.field "conversations" (D.list conversationDecoder))

init : String -> ( Model, Cmd Msg )
init flags =
    let
        maybeFlags =
            case D.decodeString initialFlagsDecoder (Debug.log "1" flags) of
                Ok value -> Just value
                Err value ->
                    let
                        a = Debug.log "3" value
                    in
                        Nothing
    in
        ({ participant =
            case Debug.log "2" maybeFlags of
                Just { participant, conversations} -> participant
                Nothing -> Nothing
        , username = ""
        , conversations =
            case maybeFlags of
                Just { participant, conversations} -> conversations
                Nothing -> []
        , query = ""
        , showCreateChatModal = False
        , user = ""
        , subject = ""
        , content = ""
        }, Cmd.none )

-- ---------------------------
-- UPDATE
-- ---------------------------

update : Msg -> Model -> ( Model, Cmd Msg )
update message model =
    case message of

        NoneMsg ->
            Debug.log "none" (model, Cmd.none)

        DisplayNewChatModal ->
            ({ model | showCreateChatModal = True }, Cmd.none)

        CloseNewChatModal ->
            ({ model | showCreateChatModal = False }, Cmd.none)

        LoadMessages conversation messages ->
            let
                updateConversation conv =
                    if (conv.id == conversation.id) then
                        { conv | messages = messages }
                    else
                        conv
                
                conversations = List.map updateConversation model.conversations
            in
                ({ model | conversations = conversations }, Cmd.none)

        NewConversation conversation ->
            ( { model | conversations = [conversation] ++ model.conversations } , Cmd.none)

        SelectConversation conversation ->
            let
                updateConversation conv =
                    if (conv.id == conversation.id) then
                        { conv | selected = True }
                    else
                        { conv | selected = False }
                
                conversations = List.map updateConversation model.conversations
            in
                ( { model | conversations = conversations }
                , toJs(
                    E.object
                        [ ("command", E.string ("conversation:" ++ String.fromInt conversation.id)) ]
                    )
                )
        
        OnQueryChange nextQuery ->
            ({ model | query = nextQuery }, Cmd.none)

        LoginUsernameChange nextUsername ->
            ({ model | username = nextUsername }, Cmd.none)
        
        Login ->
            (model
            , toJs(
                E.object
                    [ ("command", E.string "login")
                    , ("attrs", E.object
                        [ ("username", E.string model.username) ]
                    ) ]
                )
            )
        
        EventuallyLogin id conversations ->
            (({ model | participant = Just id, conversations = conversations }) , Cmd.none)

        OnChange conversation nextMessage ->
            let
                updateConversation conv =
                    if (conv.id == conversation.id) then
                        { conv | currentMessage = nextMessage }
                    else
                        conv
                
                conversations = List.map updateConversation model.conversations
            in
                ({ model | conversations = conversations }, Cmd.none)
        
        OnNewMessageUserChange nextUser ->
            ({ model | user = nextUser }, Cmd.none)
        
        OnNewMessageSubjectChange nextSubject ->
            ({ model | subject = nextSubject }, Cmd.none)
        
        OnNewMessageContentChange nextContent ->
            ({ model | content = nextContent }, Cmd.none)

        ResetNewMessageForm ->
            ({ model | content = "", user = "", subject = "" }, Cmd.none)

        SendNewMessage ->
            (model, Cmd.none)
                |> Update.andThen update EventuallySendNewMessage
                |> Update.andThen update ResetNewMessageForm
                |> Update.andThen update CloseNewChatModal
                    

        EventuallySendNewMessage ->
            ( model
            , toJs(
                E.object
                    [ ("command", E.string "create_conversation")
                    , ("attrs", E.object
                        [ ("target", E.string model.user)
                        , ("title", E.string model.subject)
                        , ("content", E.string model.content)
                        ] )
                    ]
                )
            )
        
        ReceiveMessage id msg ->
            let
                updateConversation conv =
                    if (conv.id == id) then
                        { conv | messages = conv.messages ++ [msg] }
                    else
                        conv
                
                conversations = List.map updateConversation model.conversations
            in
                ({ model | conversations = conversations }, Cmd.none)
        
        ReceiveConversations conversations ->
            ({ model | conversations = conversations }, Cmd.none)
        
        ClearConversationInput conversation ->
            let
                updateConversation conv =
                    if (conv.id == conversation.id) then
                        { conv | currentMessage = "" }
                    else
                        conv
                
                conversations = List.map updateConversation model.conversations
            in
                ({ model | conversations = conversations }, Cmd.none)
        
        SendMessage conversation ->
            (model, Cmd.none)
                |> Update.andThen update (EventuallySendMessage conversation)
                |> Update.andThen update (ClearConversationInput conversation)

        EventuallySendMessage conversation ->
            ( model
            , toJs(
                E.object
                    [ ("command", E.string "send_message")
                    , ("attrs", E.object
                        [ ("conversation_id", E.int conversation.id)
                        , ("content", E.string conversation.currentMessage)
                        ] )
                    ]
                )
            )


-- ---------------------------
-- VIEW
-- ---------------------------

view : Model -> Html Msg
view model =
    div [ id "app", class (if model.showCreateChatModal then "modal-open" else "") ]
        [ layout model]

-- ---------------------------
-- SUBSCRIPTIONS
-- ---------------------------

type IncomingMessageType =
    LoadMessagesCommand
    | CreateConversationCommand
    | ReceiveMessageCommand
    | ReceiveConversationsCommand
    | EventuallyLoginCommand


incomingMessageTypeDecoder : D.Decoder IncomingMessageType
incomingMessageTypeDecoder =
    D.string
        |> D.andThen (\str ->
            case str of
                "load_messages" -> D.succeed LoadMessagesCommand
                "create_conversation" -> D.succeed CreateConversationCommand
                "message" -> D.succeed ReceiveMessageCommand
                "conversations" -> D.succeed ReceiveConversationsCommand
                "login" -> D.succeed EventuallyLoginCommand
                somethingElse -> D.fail <| "Unknown message type " ++ somethingElse
        )

type alias IncomingMessage =
    { command: IncomingMessageType
    , payload: E.Value
    }

incomingMessageDecoder : D.Decoder IncomingMessage
incomingMessageDecoder =
    D.map2 IncomingMessage
        (D.field "command" incomingMessageTypeDecoder)
        (D.field "payload" D.value)

type alias LoadMessagesPayload =
    { id: Int
    , messages: List Message
    }

loadMessagesDecoder : D.Decoder LoadMessagesPayload
loadMessagesDecoder =
    D.map2 LoadMessagesPayload
        (D.field "id" D.int)
        (D.field "messages" (D.list messageDecoder))

type alias ReceiveMessagePayload =
    { id: Int
    , message: Message
    }

receiveMessageDecoder : D.Decoder ReceiveMessagePayload
receiveMessageDecoder =
    D.map2 ReceiveMessagePayload
        (D.field "id" D.int)
        (D.field "message" messageDecoder)

type alias EventuallyLoginPayload =
    { id: Int
    , conversations: List Conversation
    }

eventuallyLoginDecoder : D.Decoder EventuallyLoginPayload
eventuallyLoginDecoder =
    D.map2 EventuallyLoginPayload
        (D.field "id" D.int)
        (D.field "conversations" (D.list conversationDecoder))

subscriptions : Model -> Sub Msg
subscriptions model =
    toElm (
        \values ->
            let
                maybeCommand = Debug.log "1" (
                    case D.decodeString incomingMessageDecoder values of
                        Ok value -> Just value
                        Err value -> Nothing
                    )
                
            in
                case maybeCommand of
                    Nothing -> NoneMsg
                    Just { command, payload } ->
                        case command of
                            LoadMessagesCommand ->
                                let
                                    maybePayload =
                                        case D.decodeValue loadMessagesDecoder payload of
                                            Ok value -> Just value
                                            Err value -> Nothing
                                in
                                    case maybePayload of
                                        Just { id, messages } ->
                                            case List.head(List.filter (\c -> c.id == id) model.conversations) of
                                                Just conversation ->
                                                    LoadMessages conversation messages
                                                Nothing -> NoneMsg
                                        Nothing -> NoneMsg

                            CreateConversationCommand ->
                                let
                                    maybeConversation =
                                        case D.decodeValue conversationDecoder payload of
                                            Ok value -> Just value
                                            Err value -> Nothing
                                in
                                    case maybeConversation of
                                        Just conversation ->
                                            NewConversation conversation
                                        Nothing -> NoneMsg
                            
                            ReceiveConversationsCommand ->
                                let
                                    maybeConversations =
                                        case D.decodeValue (D.list conversationDecoder) payload of
                                            Ok value -> Just value
                                            Err value -> Nothing
                                in
                                    case maybeConversations of
                                        Just conversations ->
                                            ReceiveConversations conversations
                                        Nothing -> NoneMsg
                            
                            ReceiveMessageCommand ->
                                let
                                    maybePayload =
                                        case D.decodeValue receiveMessageDecoder payload of
                                            Ok value -> Just value
                                            Err value -> Nothing
                                in
                                    case maybePayload of
                                        Just { id, message } ->
                                            ReceiveMessage id message
                                        Nothing -> NoneMsg
                            
                            EventuallyLoginCommand ->
                                let
                                    maybePayload =
                                        case D.decodeValue eventuallyLoginDecoder payload of
                                            Ok value -> Just value
                                            Err value -> Nothing
                                in
                                    case Debug.log "ev-log" maybePayload of
                                        Just { id, conversations } ->
                                            EventuallyLogin id conversations
                                        Nothing -> NoneMsg
                            
                            
        )

-- ---------------------------
-- MAIN
-- ---------------------------


main : Program String Model Msg
main =
    Browser.document
        { init = init
        , update = update
        , view =
            \m ->
                { title = "Whatsapp FP"
                , body = [ view m ]
                }
        , subscriptions = subscriptions
        }