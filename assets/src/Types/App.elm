module Types.App exposing (..)

import Types.Model exposing (Conversation, Message)
import Time exposing (..)

type alias Model =
    { participant: Maybe Int
    , username: String
    , conversations : List Conversation
    , query : String
    , showCreateChatModal : Bool
    , user : String
    , subject : String
    , content : String
    }

type Msg = SelectConversation Conversation
    | OnChange Conversation String
    | OnQueryChange String
    | SendMessage Conversation
    | EventuallySendMessage Conversation
    | ClearConversationInput Conversation
    | LoadMessages Conversation (List Message)
    | DisplayNewChatModal
    | CloseNewChatModal
    | SendNewMessage
    | EventuallySendNewMessage
    | OnNewMessageUserChange String
    | OnNewMessageSubjectChange String
    | OnNewMessageContentChange String
    | ResetNewMessageForm
    | NewConversation Conversation
    | ReceiveMessage Int Message
    | ReceiveConversations (List Conversation)
    | LoginUsernameChange String
    | Login
    | EventuallyLogin Int (List Conversation)
    | NoneMsg