module Types.Model exposing (..)

import Json.Decode as D
import Json.Encode as E
import Json.Decode.Pipeline exposing (required, optional, hardcoded)

type alias Conversation =
    { id: Int
    , title: String
    -- , participants: List Participant
    -- Same as last (messages) or firt (messages)
    -- , lastMessage: Maybe Message
    -- Replaced by changing lastMessage from Maybe String to Maybe Message
    -- , lastMessageStatus: String
    -- , lastMessageDate: Date
    , iconUrl: Maybe String
    -- , unreadCount: Int
    
    -- Same as unreadCount > 0
    -- , read: Boolean
    , messages: List Message
    , created: Int
    , pinned: Bool
    , selected: Bool
    , currentMessage: String
    }

conversationDecoder : D.Decoder Conversation
conversationDecoder =
    D.succeed Conversation
        |> required "id" D.int
        |> required "title" D.string
        -- |> required "participants" (D.list participantDecoder)
        |> required "iconUrl" (D.nullable D.string)
        -- |> required "unreadCount" D.int
        
        |> required "messages" (D.list messageDecoder)
        |> required "created" D.int
        |> hardcoded False
        |> hardcoded False
        |> hardcoded ""

type alias Participant =
    { id: Int
    , name: Maybe String
    , username: String
    }

participantDecoder : D.Decoder Participant
participantDecoder =
    D.succeed Participant
        |> required "id" D.int
        |> required "name" (D.nullable D.string)
        |> required "username" D.string

type alias Message =
    { id: Int
    , content: String
    -- , readBy: List Participant
    , timestamp: Int
    -- , status: MessageStatus
    , sender: Participant 
    }

messageDecoder : D.Decoder Message
messageDecoder =
    D.succeed Message
        |> required "id" D.int
        |> required "content" D.string
        -- |> hardcoded []
        |> required "timestamp" D.int
        -- |> required "status" messageStatusDecoder
        |> required "sender" participantDecoder

type MessageStatus = Read | Unread

messageStatusDecoder : D.Decoder MessageStatus
messageStatusDecoder =
    D.string
        |> D.andThen (\str ->
            case str of
                "Read" -> D.succeed Read
                "Unread" -> D.succeed Unread
                somethingElse -> D.fail <| "Unknown status" ++ somethingElse
        )