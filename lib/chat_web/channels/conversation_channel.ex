defmodule ChatWeb.ConversationChannel do
  use ChatWeb, :channel
  
  def join("conversation:" <> conversation_id, payload, socket) do
    send(self(), :after_join)
    {
      :ok,
      %{ok: 1},
      assign(socket, :conversation_id, conversation_id)
    }
  end

  def handle_info(:after_join, socket) do
    messages = Chat.Conversation.list_messages(socket.assigns[:conversation_id])
    push(socket, "messages", %{ messages: messages })
    {:noreply, socket} # :noreply
  end

  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end
  
end
