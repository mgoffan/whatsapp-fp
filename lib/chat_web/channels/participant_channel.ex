defmodule ChatWeb.ParticipantChannel do
  use ChatWeb, :channel

  def join("participant:" <> participant_id, payload, socket) do
    if authorized?(payload) do
      send(self(), :after_join)
      {
        :ok,
        %{ ok: 1 },
        assign(socket, :participant_id, participant_id)
      }
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  def handle_info(:after_join, socket) do
    conversations = Chat.Participant.list_conversations(socket.assigns[:participant_id])
    push(socket, "init", %{ conversations: conversations })
    {:noreply, socket} # :noreply
  end

  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  def get_target(target_username) do
    target = Chat.Participant.get_participant_by_username(target_username)
    if target == nil do
      Chat.Participant.create_participant!(target_username)
    else
      target
    end
  end

  def handle_in("create_conversation", payload, socket) do
    %{
      "title" => title,
      "content" => content,
      "target" => target_username
    } = payload

    target = get_target(target_username)  
    sender = Chat.Participant.get_participant!(socket.assigns[:participant_id])

    case Chat.Conversation.create_conversation(sender, target, title, content) do
      {:ok, conversation} ->
        payload = %{ conversation: conversation }
        socket.endpoint.broadcast!("participant:#{target.id}", "create_conversation", payload)
        socket.endpoint.broadcast!("participant:#{sender.id}", "create_conversation", payload)
        
        {:reply, :ok, socket}

      {:error, _reason} ->
        {:reply, :error, socket}
    end
  end

  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (room:lobby).
  def handle_in("send_message", payload, socket) do
    %{
      "content" => content,
      "conversation_id" => conversation_id
    } = payload

    sender = Chat.Participant.get_participant!(socket.assigns[:participant_id])
    conversation = Chat.Conversation.get_conversation!(conversation_id)

    case Chat.Conversation.send_message(sender, conversation, content) do
      {:ok, message} ->
        payload = %{ message: message }
        socket.endpoint.broadcast!("conversation:#{conversation_id}", "message", payload)
        {:reply, :ok, socket}
      {:error, _reason} ->
        {:reply, :error, socket}
    end
  end

  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end
  
end
