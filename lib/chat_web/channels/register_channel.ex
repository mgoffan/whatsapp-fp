defmodule ChatWeb.RegisterChannel do
  use ChatWeb, :channel

  def join("register:" <> username, payload, socket) do
    if authorized?(payload) do
      send(self(), :after_join)
      {
        :ok,
        %{ ok: 1 },
        assign(socket, :username, username)
      }
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  def handle_info(:after_join, socket) do
    user = get_user(socket.assigns[:username])
    push(socket, "init", %{ user_id: user.id })
    {:noreply, socket} # :noreply
  end

  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  def get_user(username) do
    user = Chat.Participant.get_participant_by_username(username)
    if user == nil do
      Chat.Participant.create_participant!(username)
    else
      user
    end
  end

  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end
  
end
