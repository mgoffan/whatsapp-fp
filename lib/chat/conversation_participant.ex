defmodule Chat.ConversationParticipant do
  use Ecto.Schema
  import Ecto.Changeset


  schema "conversation_participants" do
    belongs_to :participant, Chat.Participant
    belongs_to :conversation, Chat.Conversation
  end

  def changeset(conversation_participant, attrs) do
    conversation_participant
  end
end
