defmodule Chat.Message do
  use Ecto.Schema
  import Ecto.Changeset


  schema "messages" do
    field :content, :string
    belongs_to :sender, Chat.Participant
    belongs_to :conversation, Chat.Conversation

    timestamps()
  end

  @doc false
  def changeset(message, attrs) do
    message
    |> cast(attrs, [:content])
    |> validate_required([:sender, :content, :conversation])
  end

  def get_messages(limit \\ 20) do
    Chat.Repo.all(Chat.Message, limit: limit)
  end
end
