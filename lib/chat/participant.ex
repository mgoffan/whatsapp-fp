defmodule Chat.Participant do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query


  schema "participants" do
    field :name, :string
    field :username, :string

    has_many :conversation_participants, Chat.ConversationParticipant
    has_many :conversations, through: [:conversation_participants, :conversation]

    timestamps()
  end

  @doc false
  def changeset(participant, attrs) do
    participant
    |> cast(attrs, [:name, :username])
    |> validate_required([:username])
    |> unique_constraint(:username)
  end

  def get_participant!(id), do: Chat.Repo.get!(Chat.Participant, id)
  def get_participant(id), do: Chat.Repo.get(Chat.Participant, id)

  def get_participant_by_username(username) do
    Chat.Repo.get_by(Chat.Participant, username: username)
  end

  def create_participant!(username) do
    Chat.Participant.changeset(%Chat.Participant{}, %{ username: username }) |> Chat.Repo.insert!
  end

  def list_conversations(participant_id) do
    Chat.Repo.all(
      from cp in Chat.ConversationParticipant,
      where: cp.participant_id == ^participant_id,
      join: c in assoc(cp, :conversation),
      join: m in assoc(c, :last_message),
      join: s in assoc(m, :sender),
      order_by: [asc: m.inserted_at],
      distinct: true,
      select: %{
        id: c.id,
        title: c.title,
        iconUrl: c.iconUrl,
        created: c.inserted_at,
        messages: [%{
          id: m.id,
          content: m.content,
          timestamp: m.inserted_at,
          sender: %{
            id: s.id,
            name: s.name,
            username: s.username
          }
        }]
      }
    )
  end
end
