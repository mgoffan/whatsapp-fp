defmodule Chat.Conversation do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query


  schema "conversations" do
    field :iconUrl, :string
    field :title, :string

    belongs_to :last_message, Chat.Message
    
    has_many :messages, Chat.Message
    has_many :conversation_participants, Chat.ConversationParticipant
    has_many :participants, through: [:conversation_participants, :participant]

    timestamps()
  end

  @doc false
  def changeset(conversation, attrs) do
    conversation
    |> cast(attrs, [:title, :iconUrl])
    |> validate_required([:title])
  end

  def get_conversation!(id), do: Chat.Repo.get!(Chat.Conversation, id)
  def get_conversation(id), do: Chat.Repo.get(Chat.Conversation, id)

  def list_messages(conversation_id, limit \\ 15) do
    Chat.Repo.all(
      from msg in Chat.Message,
      where: msg.conversation_id == ^conversation_id,
      join: participant in assoc(msg, :sender),
      order_by: [asc: msg.inserted_at],
      limit: ^limit,
      select: %{
        id: msg.id,
        content: msg.content,
        timestamp: msg.inserted_at,
        sender: %{
          id: participant.id,
          name: participant.name,
          username: participant.username
        }
      }
    )
  end

  def create_conversation(sender, target, title, content) do
    Chat.Repo.transaction fn ->
      conversation = Chat.Conversation.changeset(%Chat.Conversation{}, %{ title: title }) |> Chat.Repo.insert!

      cp1 = %Chat.ConversationParticipant{
        participant_id: sender.id,
        conversation_id: conversation.id
      } |> Chat.Repo.insert!

      cp2 = %Chat.ConversationParticipant{
        participant_id: target.id,
        conversation_id: conversation.id
      } |> Chat.Repo.insert!

      message = %Chat.Message{
        content: content,
        sender_id: sender.id,
        conversation_id: conversation.id
      } |> Chat.Repo.insert!

      conversation |> Ecto.Changeset.change(%{ last_message_id: message.id }) |> Chat.Repo.update!
      
      %{
        id: conversation.id,
        title: conversation.title,
        iconUrl: conversation.iconUrl,
        created: conversation.inserted_at,
        messages: [%{
          id: message.id,
          content: message.content,
          timestamp: message.inserted_at,
          sender: %{
            id: sender.id,
            name: sender.name,
            username: sender.username
          }
        }]
      }
    end
  end

  def send_message(sender, conversation, content) do
    Chat.Repo.transaction fn ->
      message = %Chat.Message{
        content: content,
        sender_id: sender.id,
        conversation_id: conversation.id
      } |> Chat.Repo.insert!

      conversation |> Ecto.Changeset.change(%{ last_message_id: message.id }) |> Chat.Repo.update!
      
      %{
        id: message.id,
        content: message.content,
        timestamp: message.inserted_at,
        sender: %{
          id: sender.id,
          name: sender.name,
          username: sender.username
        }
      }
    end
  end
end
