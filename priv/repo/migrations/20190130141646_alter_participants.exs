defmodule Chat.Repo.Migrations.AlterParticipants do
  use Ecto.Migration

  def change do
    rename table(:participants), :phone, to: :username
  end
end
