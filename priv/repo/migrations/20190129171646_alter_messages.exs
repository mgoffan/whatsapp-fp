defmodule Chat.Repo.Migrations.AlterMessages do
  use Ecto.Migration

  def change do
    alter table(:messages) do
      add :conversation, references(:conversations)
    end
  end
end
