defmodule Chat.Repo.Migrations.AlterMessages do
  use Ecto.Migration

  def change do
    rename table(:messages), :sender, to: :sender_id
    rename table(:messages), :conversation, to: :conversation_id
  end
end
