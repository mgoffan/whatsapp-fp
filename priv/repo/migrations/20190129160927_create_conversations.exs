defmodule Chat.Repo.Migrations.CreateConversations do
  use Ecto.Migration

  def change do
    create table(:conversations) do
      add :title, :string
      add :iconUrl, :string

      timestamps()
    end

  end
end
