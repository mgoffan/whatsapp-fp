defmodule Chat.Repo.Migrations.AlterConversations do
  use Ecto.Migration

  def change do
    rename table(:conversations), :last_message, to: :last_message_id
  end
end
