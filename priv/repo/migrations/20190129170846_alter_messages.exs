defmodule Chat.Repo.Migrations.AlterMessages do
  use Ecto.Migration

  def change do
    rename table(:messages), :message, to: :content
  end
end
