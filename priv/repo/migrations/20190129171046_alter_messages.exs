defmodule Chat.Repo.Migrations.AlterMessages do
  use Ecto.Migration

  def change do
    alter table(:messages) do
      remove :name
      add :sender, references(:participants)
    end
  end
end
