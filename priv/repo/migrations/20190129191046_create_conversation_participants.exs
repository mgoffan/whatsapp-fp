defmodule Chat.Repo.Migrations.CreateConversationParticipants do
  use Ecto.Migration

  def change do
    create table(:conversation_participants) do
      add :conversation_id, references(:conversations)
      add :participant_id, references(:participants)
    end
  end
end
