defmodule Chat.Repo.Migrations.AlterConversations do
  use Ecto.Migration

  def change do
    alter table(:conversations) do
      add :last_message, references(:messages)
    end
  end
end
